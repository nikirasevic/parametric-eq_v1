# Description

In this project was implemented Parametric Equalizer with KHN filter topology. The idea was to compare results from mathematical calculation, computer simulation and circuit measurement. 
The results are described in the documentation.

## Block Diagram
![Block Diagram](/Docs/img/block-diagram.png)

## PCB
![PCB](/Docs/img/pcb-altium.PNG)

## Simulation Results
![Simulation Results](/Docs/img/bode-plot-sim.png)

## Measurement Results
![Measurement Results](/Docs/img/measurement-output.PNG)

